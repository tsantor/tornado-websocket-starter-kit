# -*- coding: utf-8 -*-

import logging
from datetime import datetime
import uuid
from tornado.ioloop import IOLoop
from tornado.websocket import WebSocketHandler

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


class WebSocketHandler(WebSocketHandler):
    """When a client connects an instance of this handler is spawned."""

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        self.io_loop = IOLoop.current()

    def initialize(self, callback, client_manager, drop_dupes=False):
        self.callback = callback
        self.client_manager = client_manager
        self.drop_dupes = drop_dupes
        self.uid = uuid.uuid4().hex

    def check_origin(self, origin):
        """By default, tornado rejects all requests with an origin on
        a host other than this one. Here, we force `check_origin` to
        return True so request can come from anywhere."""
        # parsed_origin = urllib.parse.urlparse(origin)
        # return parsed_origin.netloc.endswith(".mydomain.com")
        return True

    def to_obj(self):
        """Used to get the object properties in the dashboard template."""
        return {
            "name": self.id,
            "ip": self.ip,
            "port": self.port,
            "ping": self.ping_time,
            "time_online": self.time_online,
            "uid": self.uid,
        }

    def open(self, *args):
        """When a client socket is opened."""
        self.id = self.get_argument("id")

        self.set_nodelay(True)

        # self.ip = self.request.remote_ip
        # try:
        #     self.port = self.request.connection.stream.socket.getpeername()[1]
        # except AttributeError:
        self.ip, self.port = self.request.connection.context.address

        self.ini_time = datetime.now()
        self.ping_time = 0

        # Do not allow a client id to connect more than once
        if self.drop_dupes and self.client_manager.exists(self):
            existing_client = self.client_manager.get_by_id(self.id)
            logger.warning(
                "There is already a client %s. Rejecting connection from %s.",
                existing_client.identifier,
                self.identifier,
            )
            self.close()
            return

        self.client_manager.add(self)

        logger.info("Client connected: %s", self.identifier)
        logger.info("Clients connected: %s", len(self.client_manager.clients))

    @property
    def identifier(self):
        """Return string identifier for logging."""
        return f"{self.id} ({self.ip}:{self.port})"

    @property
    def time_online(self):
        """Calculate the time this client has been online."""
        delta = datetime.now() - self.ini_time
        s = delta.total_seconds()
        hours, remainder = divmod(s, 3600)
        minutes, seconds = divmod(remainder, 60)
        return "%02d:%02d:%02d" % (hours, minutes, seconds)

    def on_ping(self, data):
        """Invoked when a ping frame is received."""
        # logger.debug('Received ping: "%s" from %s' % (data, self.identifier))
        pass

    def on_pong(self, data):
        """Invoked when the response to a ping frame is received."""
        # logger.debug('Received pong: "%s" from %s' % (data, self.identifier))

        if self.ws_connection:
            # Calculate ping time
            now = self.io_loop.time()
            self.ping_time = round((now - self.ws_connection.last_ping) * 1000, 3)
            # logger.debug('Ping time for {}: {} ms'.format(self.identifier, self.ping_time))

    def on_message(self, message):
        """When a message is received."""
        self.callback(self, message)

    def on_close(self):
        """When a client socket is closed."""
        self.client_manager.remove(self)

        logger.info(
            "Client disconnected: %s. Total time online: %s",
            self.identifier,
            self.time_online,
        )
        logger.info("Clients connected: %s", len(self.client_manager.clients))
