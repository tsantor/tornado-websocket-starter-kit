# -*- coding: utf-8 -*-

import json
import logging

from tornado.websocket import WebSocketClosedError

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


class MessageFormatError(Exception):
    """Raised when a message is not the proper JSON format of:
    {"type":"foo", "data":"bar"}."""

    pass


def message_handler(client, msg):
    """Determines how messages are handled."""
    try:
        obj = json.loads(msg)
        if "type" not in obj:
            raise MessageFormatError()
    except (ValueError, TypeError, MessageFormatError) as e:
        logger.error("Invalid message received from %s: %s", client.identifier, msg)
        return

    if obj["type"] == "Handshake":
        client.id = obj["data"]
        logger.debug("Handshake message received from %s", client.identifier)

    elif obj["type"] == "Ping":
        client.on_ping()

    elif obj["type"] == "Pong":
        client.on_pong()

    elif obj["type"] == "KeepAlive":
        logger.debug("Keep Alive message received from %s", client.identifier)

    else:
        logger.debug("Unhandled message received from %s: %s", client.identifier, msg)
