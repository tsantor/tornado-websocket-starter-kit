/* global $ */
'use strict';

$(function () {

    $('#clients').DataTable({
        'paging': false,
        'ordering': false,
    });

    function showConnected(elem, data) {
        var id = elem.attr('id');
        $('#' + id + ' .ip').html(data[id]['ip']);
        $('#' + id + ' .port').html(data[id]['port']);
        $('#' + id + ' .ping').html(data[id]['ping'] + ' ms');
        $('#' + id + ' .status #connected').css('display', 'inline');
        $('#' + id + ' .status #not-connected').css('display', 'none');
        $('#' + id + ' .time').html(data[id]['time_online']);
        $('#' + id + ' .scan-form').css('display', 'block');
    }

    function showDisconnected(elem) {
        var id = elem.attr('id');
        $('#' + id + ' .ip').html('');
        $('#' + id + ' .port').html('');
        $('#' + id + ' .ping').html('');
        $('#' + id + ' .status #connected').css('display', 'none');
        $('#' + id + ' .status #not-connected').css('display', 'inline');
        // $('#'+id+ ' .time').html('');
        $('#' + id + ' .scan-form').css('display', 'none');
    }

    // get status of all readpoints
    function update() {
        $.get('/update', function (data, status) {
            // loop through each readpoint in the list and update it
            $('table tr.readpoint').each(function () {
                var id = $(this).attr('id');
                if (data.hasOwnProperty(id)) {
                    showConnected($(this), data);
                } else {
                    showDisconnected($(this));
                }
            });
        }).fail(function (jqXHR, textStatus, error) {
            $('table tr.readpoint').each(function () {
                showDisconnected($(this));
            });
        });
    }

    update();
    var refresh_interval = setInterval(update, 3000);

    // update refresh rate
    $('input[name="refresh"]').on('change', function () {
        // console.log($(this).val());
        clearInterval(refresh_interval);
        refresh_interval = setInterval(update, $(this).val() * 1000);
    });

    function getCookie(name) {
        var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
        return r ? r[1] : undefined;
    }

    $(document).on('click', 'button[type="submit"]', function (e) {
        e.preventDefault();
        // var form = $(this).closest('form');
        //var formData = form.serializeArray();

        var data = {
            // '_xsrf': getCookie('_xsrf'),
            'readPoint': $(this).siblings('input[name="readPoint"]').val(),
            'epcData': $('#epcData').val(),
        }

        var formData = JSON.stringify(data);
        // console.log(formData);

        $.ajax({
            url: 'http://' + window.location.hostname + ':' + window.location.port + '/AVService/RfidTagNotifications/',
            type: 'POST',
            data: formData,
            dataType: 'json',
            contentType: 'application/json',
            success: function (response) {
                console.log(response);
            },
            error: function (response) {
                var response = JSON.parse(response.responseText);
                console.warn(response);
            }
        });
    });

});
