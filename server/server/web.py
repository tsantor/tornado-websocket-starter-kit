# -*- coding: utf-8 -*-

# import pkg_resources
import json

from tornado.web import RequestHandler

# -----------------------------------------------------------------------------


class DefaultHandler(RequestHandler):
    """Default handler"""

    def get(self):
        self.write("These are not the droids you are looking for...")
        # template = pkg_resources.resource_filename(__name__, 'templates/client-test.html')
        # self.render(template)
        self.finish()


class DashboardHandler(RequestHandler):
    """Dashboard handler"""

    def initialize(self, readpoints, clients):
        self.readpoints = readpoints
        self.clients = clients

    # @tornado.web.authenticated()
    def get(self):
        # template = pkg_resources.resource_filename(__name__, 'templates/dashboard.html')
        # template = os.path.join(os.path.dirname(__file__), 'templates/dashboard.html')
        template = "dashboard.html"
        self.render(template, readpoints=self.readpoints, clients=self.clients)


class DashboardUpdateHandler(RequestHandler):
    """Dashboard update handler"""

    def initialize(self, clients):
        self.clients = clients

    # @tornado.web.authenticated()
    def get(self):
        self.set_header("Content-Type", "application/json")
        obj = {c.uid: c.to_obj() for c in self.clients}
        self.write(json.dumps(obj))
