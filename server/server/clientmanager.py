# -*- coding: utf-8 -*-

import logging

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


class ClientManager:
    """Class whose sole job is to manage client connections."""

    clients = set()

    def __init__(self, **kwargs):
        pass

    def add(self, handler):
        """Add a client."""
        self.clients.add(handler)

    def remove(self, handler):
        """Remove a client."""
        if handler in self.clients:
            self.clients.remove(handler)

    def exists(self, handler):
        """Returns True if a client with the same ID exists."""
        return any(c.id == handler.id for c in self.clients)

    def get_by_id(self, id):
        """Get client by ID. We go in reverse to get the newest/last client
        that used this ID."""
        for c in reversed(list(self.clients)):
            if c.id == id:
                return c

    def count_by_id(self, id):
        """Get client count by ID."""
        return sum(c.id == id for c in self.clients)

    def list_clients(self):
        return [c.id for c in self.clients]
