# -*- coding: utf-8 -*-

# import pkg_resources
import logging
import os
import signal
import uuid

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import Application

from server import config
from server.clientmanager import ClientManager
from server.message import message_handler
from server.web import DashboardHandler, DashboardUpdateHandler
from server.websocket import WebSocketHandler

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------

client_manager = ClientManager()


def _handle_shutdown_signals(signum, frame):
    assert signum in (signal.SIGINT, signal.SIGTERM)
    logger.info("signal -%d received", signum)
    logger.info("Received graceful shutdown request")
    IOLoop.current().stop()


def get_tornado_app():
    """Return a tornado web application instance which includes url config"""
    # Safely get some configuration options
    ping_interval = config.getint("default", "ping_interval")
    ping_timeout = None
    if config.has_option("default", "ping_timeout"):
        ping_timeout = config.getint("default", "ping_timeout")
        if ping_timeout < ping_interval:
            raise Exception("ping_timeout must be greater than ping_interval")

    settings = dict(
        cookie_secret=uuid.uuid4().hex,
        # xsrf_cookies=True,
        template_path=os.path.join(os.path.dirname(__file__), "server/templates"),
        static_path=os.path.join(os.path.dirname(__file__), "server/static"),
        debug=True,
        websocket_ping_interval=ping_interval,
        websocket_ping_timeout=ping_timeout,
    )

    urls = [
        # (r'/', DefaultHandler),
        (
            r"/",
            DashboardHandler,
            dict(
                readpoints=[],  # config.items('read_points'),
                clients=client_manager.clients,
            ),
        ),
        (r"/update", DashboardUpdateHandler, dict(clients=client_manager.clients)),
        (
            r"/ws/",
            WebSocketHandler,
            dict(
                callback=message_handler,
                client_manager=client_manager,
                drop_dupes=config.getboolean("default", "drop_dupes"),
            ),
        ),
    ]

    return Application(urls, **settings)


def main():
    """Run script."""
    # Things seem to fail more gracefully if we trigger the stop
    # out of band (with a signal handler) instead of catching the
    # KeyboardInterrupt...
    signal.signal(signal.SIGINT, _handle_shutdown_signals)
    signal.signal(signal.SIGTERM, _handle_shutdown_signals)

    # Get config items
    host = config.get("default", "ip")
    port = config.getint("default", "port")

    # Create Tornado application
    app = get_tornado_app()
    httpserver = HTTPServer(app)
    httpserver.listen(port, address=host)

    logger.debug("Listening on %s:%d...", host, port)

    IOLoop.current().start()

    httpserver.stop()
    logger.info("Server shut down gracefully")


if __name__ == "__main__":
    main()
