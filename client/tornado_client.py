# -*- coding: utf-8 -*-

import logging
import signal

from tornado.ioloop import IOLoop

from client import config
from client.message import message_handler
from client.websocket import WebsocketClient

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


def _handle_shutdown_signals(signum, frame):
    """Handle shutdown signals."""
    assert signum in (signal.SIGINT, signal.SIGTERM)
    logger.info("signal -%d received", signum)
    logger.info("Received graceful shutdown request")
    IOLoop.current().stop()


def main():
    # Things seem to fail more gracefully if we trigger the stop
    # out of band (with a signal handler) instead of catching the
    # KeyboardInterrupt...
    signal.signal(signal.SIGINT, _handle_shutdown_signals)
    signal.signal(signal.SIGTERM, _handle_shutdown_signals)

    # Get config items
    host = config.get("default", "ip")
    port = config.getint("default", "port")
    name = config.get("default", "id")

    ping_interval = config.getint("default", "ping_interval")
    ping_timeout = None
    if config.has_option("default", "ping_timeout"):
        ping_timeout = config.getint("default", "ping_timeout")
        if ping_timeout < ping_interval:
            raise Exception("ping_timeout must be greater than ping_interval")

    keepalive_interval = config.getint("default", "keepalive_interval")
    if keepalive_interval < 30 and keepalive_interval != 0:
        raise Exception("Config file `keepalive_interval` value must be >= 30")

    # Websocket client
    ws_url = "ws://{host}:{port}/ws/?id={id}".format(host=host, port=port, id=name)
    client = WebsocketClient(
        ws_url, message_handler, ping_interval, ping_timeout, keepalive_interval
    )

    IOLoop.current().start()

    client.disconnect()
    logger.debug("Client shut down gracefully")


if __name__ == "__main__":
    main()
