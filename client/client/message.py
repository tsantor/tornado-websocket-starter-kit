# -*- coding: utf-8 -*-

import json
import logging

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


class MessageFormatError(Exception):
    """Raised when a message is not the proper JSON format of:
    {"type":"foo", "data":"bar"}."""

    pass


def message_handler(client, msg):
    """Handle incoming messages."""
    try:
        obj = json.loads(msg)
        if "type" not in obj:
            raise MessageFormatError()
    except (ValueError, TypeError, MessageFormatError) as e:
        logger.warning("Invalid message received from server: %s", msg)
        return

    if obj["type"] == "Pong":
        logger.debug("Pong message received from server")

    elif obj["type"] == "KeepAlive":
        logger.debug("KeepAlive message received from server")

    else:
        logger.debug("Unhandled message received from server: %s", msg)
