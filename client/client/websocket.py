# -*- coding: utf-8 -*-

import logging
from datetime import datetime
from urllib.parse import parse_qs, urlparse

from tornado import gen
from tornado.ioloop import IOLoop, PeriodicCallback
from tornado.websocket import websocket_connect
logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


class WebsocketClient:
    """
    Client websocket

    The PeriodicCallback keep_alive will periodically send a message to the
    server every X seconds. If the connection is closed, it will attempt to reconnect.

    ping_interval and ping_timeout work together to constantly ping the server.
    If the ping times out, it will close the connection.
    """

    def __init__(
        self,
        url,
        message_handler,
        ping_interval=0,
        ping_timeout=None,
        keepalive_interval=None,
    ):
        self.url = url
        self.message_handler = message_handler
        self.ping_interval = ping_interval
        self.ping_timeout = ping_timeout

        parsed = urlparse(self.url)
        params = parse_qs(parsed.query)
        self.id = params["id"][0]

        self.ioloop = IOLoop.current()
        self.ws = None

        self.connect()
        if keepalive_interval:
            PeriodicCallback(
                self.keep_alive,
                keepalive_interval * 1000,
            ).start()

    # @property
    # def identifier(self):
    #     """Return string identifier for logging."""
    #     return f"{self.id} ({self.ip}:{self.port})"

    @property
    def time_online(self):
        """Calculate the time this client has been online."""
        delta = datetime.now() - self.ini_time
        s = delta.total_seconds()
        hours, remainder = divmod(s, 3600)
        minutes, seconds = divmod(remainder, 60)
        return "%02d:%02d:%02d" % (hours, minutes, seconds)

    @gen.coroutine
    def connect(self):
        """Connect."""
        logger.debug("Trying to connect to %s", self.url)
        try:
            self.ws = yield websocket_connect(
                self.url,
                # on_message_callback=self.message_callback,
                ping_interval=self.ping_interval,
                ping_timeout=self.ping_timeout,
            )
            # self.ip, self.port = self.ws.stream.socket.getpeername()
            self.ini_time = datetime.now()
        except Exception as e:
            logger.warning("Connection error: %s", e)
        else:
            logger.debug('Connected as "%s"', self.id)
            self.run()

    def disconnect(self):
        """Disconnect."""
        if self.ws:
            self.ws.close()
            self.ws = None

    @gen.coroutine
    def run(self):
        """Run the client."""
        while True:
            msg = yield self.ws.read_message()
            if msg is None:
                logger.info(
                    "Connection closed. Total time online: %s", self.time_online
                )
                self.ws = None
                break
            self.message_handler(self, msg)

    def keep_alive(self):
        """Keep connection alive."""
        if self.ws is None:
            self.connect()
