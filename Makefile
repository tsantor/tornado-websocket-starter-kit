env:
	pyenv virtualenv 3.9.4 tornado_env && pyenv local tornado_env

reqs:
	python -m pip install -U pip black pylint
	# pre-commit && pre-commit install
	python -m pip install -r requirements.txt

serve:
	cd server && python tornado_server.py --config=server.cfg

listen:
	cd client && python tornado_client.py --config=client.cfg
